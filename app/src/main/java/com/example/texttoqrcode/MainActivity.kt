package com.example.texttoqrcode

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.tooling.preview.Preview
import com.example.texttoqrcode.ui.theme.TextToQRCodeTheme

// The following needed for QR Code generation
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        //Example JSON array containing 2 menu items
        val egJsonArray = """ 
                       [
                          {"menu": "Favourite healthy meal", "price": 12.00, "Description": "Very tasty"},
                          {"menu": "Good priced meal", "price": 10.00, "Description": "Kids favourite"}
                        ]
                       """.trimIndent()

        super.onCreate(savedInstanceState)
        setContent {
            TextToQRCodeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val bitmap = generateQRCode(inputStr = egJsonArray, screenWidth = 512, screenHeight = 512)

                   Image(bitmap!!.asImageBitmap(), contentDescription = null)
                }
            }
        }
    }
}


@Composable
/*Uses Zxing Barcode scanner library for Android
  https://github.com/journeyapps/zxing-android-embedded
 */
fun generateQRCode(inputStr: String, screenWidth: Int, screenHeight: Int):Bitmap? {

    val writer = QRCodeWriter()
    val bitMatrix = writer.encode(inputStr, BarcodeFormat.QR_CODE, screenWidth, screenHeight)
    val width = bitMatrix.width
    val height = bitMatrix.height
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until width) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)

            }
        }
    return bitmap
    }


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    val egJsonArray = """ 
                       [
                          {"menu": "Favourite healthy meal", "price": 12.00, "Description": "Very tasty"},
                          {"menu": "Good priced meal", "price": 10.00, "Description": "Kids favourite"}
                        ]
                       """.trimIndent()
    TextToQRCodeTheme {
        val bitmap = generateQRCode(inputStr = egJsonArray, screenWidth = 512, screenHeight = 512)
        Image(bitmap!!.asImageBitmap(), contentDescription = null)
    }
}